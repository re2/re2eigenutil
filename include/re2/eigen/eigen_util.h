/*
 * EigenHelper.h
 *
 *  Created on: Apr 9, 2012
 *      Author: somervil
 *
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef RE2_EIGENHELPER_H
#define RE2_EIGENHELPER_H


#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <Eigen/Core>

#include <boost/shared_ptr.hpp>
#include <boost/type_traits/remove_const.hpp>

#include <list>
#include <vector>

#define EIGEN_MAKE_GENERIC_TYPEDEF(Class, Type, TypeSuffix, Size, SizeSuffix) \
typedef Class<Type, Size>  Class##SizeSuffix##TypeSuffix;

#define EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, Type, TypeSuffix) \
EIGEN_MAKE_GENERIC_TYPEDEF(Class, Type, TypeSuffix, 2, 2) \
EIGEN_MAKE_GENERIC_TYPEDEF(Class, Type, TypeSuffix, 3, 3) \
EIGEN_MAKE_GENERIC_TYPEDEF(Class, Type, TypeSuffix, 4, 4)

#define EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_TYPES_ALL_SIZES(Class) \
EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, int,                  i)  \
EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, float,                f)  \
EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, double,               d)  \
EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, std::complex<float>,  cf) \
EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_SIZES(Class, std::complex<double>, cd)



namespace Eigen
{
    static const double             NanD = std::numeric_limits<double>::quiet_NaN();
    static const Eigen::Vector3d    Nan3d( NanD, NanD, NanD );
    static const Eigen::Quaterniond NanQuatd( NanD, NanD, NanD, NanD );
    static const Eigen::Vector3d    Zero3d( 0, 0, 0 );

    static const Eigen::Vector3d    XAxis3d( 1, 0, 0 );
    static const Eigen::Vector3d    YAxis3d( 0, 1, 0 );
    static const Eigen::Vector3d    ZAxis3d( 0, 0, 1 );

    enum AxisIndex
    {
        XAxisIndex = 0,
        YAxisIndex = 1,
        ZAxisIndex = 2
    };

    template< typename TypeT >
    struct Consts;

    template <>
    struct Consts <Vector3d>
    {
        static const Vector3d Zero;
        static const Vector3d Nan;
        static const Vector3d Infinity;
        static const Vector3d XAxis;
        static const Vector3d YAxis;
        static const Vector3d ZAxis;
    };


#ifdef RE2_EIGENHELPER_H_DEFINITION
    const Vector3d Consts<Vector3d>::Zero     = Vector3d(    0,   0,   0 );
    const Vector3d Consts<Vector3d>::Nan      = Vector3d( NanD,NanD,NanD );
    const Vector3d Consts<Vector3d>::Infinity = Vector3d( std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(),std::numeric_limits<double>::infinity() );
    const Vector3d Consts<Vector3d>::XAxis    = Vector3d(    1,   0,   0 );
    const Vector3d Consts<Vector3d>::YAxis    = Vector3d(    0,   1,   0 );
    const Vector3d Consts<Vector3d>::ZAxis    = Vector3d(    0,   0,   1 );
#endif


    template <>
    struct Consts <Eigen::Quaterniond>
    {
        static const Quaterniond Nan;
    };

#ifdef RE2_EIGENHELPER_H_DEFINITION
    const Quaterniond Consts<Quaterniond>::Nan   = Quaterniond( NanD,NanD,NanD,NanD );
#endif


    template< typename VectorT >
    class LineSegment
    {
        public:
            static const int Dim = VectorT::SizeAtCompileTime;

            typedef typename VectorT::Scalar                  ScalarT;
            typedef ParametrizedLine<ScalarT, Dim>            ParametrizedLineT;

            LineSegment( const VectorT & p0arg, const VectorT & p1arg ) : m_p0( p0arg ), m_p1( p1arg ) {}
            LineSegment()                                               : m_p0( Consts<VectorT>::Nan ), m_p1( Consts<VectorT>::Nan ) {} //FIXME: too specialized

            VectorT & p0()         { return m_p0; }
            VectorT & p1()         { return m_p1; }

            VectorT   p0()   const { return m_p0; }
            VectorT   p1()   const { return m_p1; }

            VectorT   diff() const { return (p1() - p0()); }

            ParametrizedLineT line() const { return ParametrizedLineT::Through( p0(), p1() ); }

        private:
            VectorT m_p0;
            VectorT m_p1;

        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };


    template< typename ScalarT > //, int Dim >
    struct Pose3
    {
        public:
            static const int Dim = 3;
            typedef Eigen::Matrix<ScalarT,Dim,1> VectorType;
            typedef Eigen::Quaternion<ScalarT>   QuaternionType;

            Pose3( const VectorType & positionA, const QuaternionType & orientationA )
              : position( positionA ), orientation( orientationA )
            {}

            Pose3()
              : position( Consts<VectorType>::Nan ), orientation( Consts<QuaternionType>::Nan )
            {}

            VectorType     position;
            QuaternionType orientation;

        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };

    template< typename ScalarT >
    class Twist
    {
        public:
            static const int Dim = 3;
            typedef Eigen::Matrix<ScalarT,Dim,1> VectorType;

            Twist()
                : translation( Consts<VectorType>::Nan ), rotation( Consts<VectorType>::Nan )
            {}

            VectorType translation;
            VectorType rotation;

        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    };

    typedef Twist<double>                                           Twistd;

    typedef std::vector<Twistd,Eigen::aligned_allocator<Twistd> >   TwistdVector;
    typedef boost::shared_ptr<TwistdVector>                         TwistdVectorPtr;


    typedef Pose3<float>                                            Pose3f;
    typedef Pose3<double>                                           Pose3d;

    typedef std::vector<Pose3d>                                     Pose3dVector;
    typedef boost::shared_ptr<Pose3dVector>                         Pose3dVectorPtr;

    EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_TYPES_ALL_SIZES(ParametrizedLine)
    EIGEN_MAKE_GENERIC_TYPEDEFS_ALL_TYPES_ALL_SIZES(Hyperplane)

    typedef std::vector<ParametrizedLine3d>                         ParametrizedLine3dVector;

    typedef LineSegment<Vector2d>                                   LineSegment2d;

    typedef LineSegment<Vector3d>                                   LineSegment3d;
    typedef std::vector<LineSegment3d>                              LineSegment3dVector;
    typedef boost::shared_ptr<LineSegment3dVector>                  LineSegment3dVectorPtr;
    typedef boost::shared_ptr<LineSegment3dVector const>            LineSegment3dVectorConstPtr;

    typedef Eigen::aligned_allocator<Eigen::Vector2d>               Vector2dAllignedAllocator;
    typedef Eigen::aligned_allocator<Eigen::Vector3d>               Vector3dAllignedAllocator;
    typedef Eigen::aligned_allocator<Eigen::Vector4d>               Vector4dAllignedAllocator;

    typedef Eigen::aligned_allocator<Eigen::Vector2f>               Vector2fAllignedAllocator;
    typedef Eigen::aligned_allocator<Eigen::Vector3f>               Vector3fAllignedAllocator;
    typedef Eigen::aligned_allocator<Eigen::Vector4f>               Vector4fAllignedAllocator;

    typedef Eigen::aligned_allocator<Eigen::Affine3d>               Affine3dAllignedAllocator;

    typedef std::vector<Eigen::Vector2d,Vector2dAllignedAllocator>  Vector2dVector;
    typedef std::vector<Eigen::Vector3d,Vector3dAllignedAllocator>  Vector3dVector;
    typedef std::vector<Eigen::Vector4d,Vector4dAllignedAllocator>  Vector4dVector;

    typedef std::vector<Eigen::Vector2f,Vector2fAllignedAllocator>  Vector2fVector;
    typedef std::vector<Eigen::Vector3f,Vector3fAllignedAllocator>  Vector3fVector;
    typedef std::vector<Eigen::Vector4f,Vector4fAllignedAllocator>  Vector4fVector;

    typedef boost::shared_ptr<Vector2dVector>                       Vector2dVectorPtr;
    typedef boost::shared_ptr<Vector3dVector>                       Vector3dVectorPtr;
    typedef boost::shared_ptr<Vector4dVector>                       Vector4dVectorPtr;

    typedef boost::shared_ptr<Vector2fVector>                       Vector2fVectorPtr;
    typedef boost::shared_ptr<Vector3fVector>                       Vector3fVectorPtr;
    typedef boost::shared_ptr<Vector4fVector>                       Vector4fVectorPtr;

    typedef std::list<Eigen::Vector2d,Vector2dAllignedAllocator>    Vector2dList;
    typedef std::list<Eigen::Vector3d,Vector3dAllignedAllocator>    Vector3dList;
    typedef std::list<Eigen::Vector4d,Vector4dAllignedAllocator>    Vector4dList;

    typedef boost::shared_ptr<Vector2dList>                         Vector2dListPtr;
    typedef boost::shared_ptr<Vector3dList>                         Vector3dListPtr;
    typedef boost::shared_ptr<Vector4dList>                         Vector4dListPtr;


    typedef std::list<Eigen::Vector3f, Vector3fAllignedAllocator>   Vector3fList;
    typedef boost::shared_ptr<Vector3fList>                         Vector3fListPtr;
    typedef boost::shared_ptr<Vector3fVector>                       Vector3fVectorPtr;

    typedef Eigen::aligned_allocator<Eigen::VectorXf>               VectorXfEigenAllocator;
    typedef std::vector< Eigen::VectorXf, VectorXfEigenAllocator>   VectorXfVector;

    typedef Eigen::aligned_allocator<Eigen::MatrixXd>               MatrixXdEigenAllocator;
    typedef std::vector< Eigen::MatrixXd, MatrixXdEigenAllocator>   MatrixXdVector;
    typedef std::list< Eigen::MatrixXd, MatrixXdEigenAllocator>     MatrixXdList;

    typedef Eigen::Map<Eigen::Vector2d>                             Vector2dMap;
    typedef Eigen::Map<const Eigen::Vector2d>                       Vector2dConstMap;

    typedef Eigen::Map<Eigen::Vector3d>                             Vector3dMap;
    typedef Eigen::Map<const Eigen::Vector3d>                       Vector3dConstMap;
    typedef Eigen::Map<Eigen::Matrix3d>                             Matrix3dMap;
    typedef Eigen::Matrix<double,6,1>                               Vector6d;
    typedef boost::shared_ptr<Vector6d>                             Vector6dPtr;


    typedef std::list<Eigen::Affine3d, Affine3dAllignedAllocator>   Affine3dList;
    typedef std::vector<Eigen::Affine3d, Affine3dAllignedAllocator> Affine3dVector;
    typedef boost::shared_ptr<Affine3dList>                         Affine3dListPtr;
    typedef boost::shared_ptr<Affine3dVector>                       Affine3dVectorPtr;

}


namespace re2
{
    template< typename LineT, typename PlaneT>
    typename LineT::VectorType
    intersectLineWithPlane( const LineT & line, const PlaneT & plane )
    {
        // work around known libeigen const problem (fixed in latest eigen)
        typedef typename boost::remove_const<LineT>::type NonConstLineT;
        NonConstLineT & nonConstLine = const_cast<NonConstLineT &>(line);

        double t = nonConstLine.intersection( plane );
        return (line.origin() + t*line.direction());
    }


    template< typename LineT, typename PlaneT>
    typename LineT::VectorType
    approximateIntersection( const LineT & line0, const LineT & line1 )
    {
        typedef typename LineT::Scalar                  Scalar;
        const int Dims = LineT::AmbientDimAtCompileTime;

        Eigen::Hyperplane<Scalar, Dims> line0Plane( line1.direction(), line0.origin() );

        return intersectLineWithPlane( line1, line0Plane );
    }


    template<typename LineT, typename XprType>
    inline
    LineT&
    transform( const LineT & lineArg, const Eigen::MatrixBase<XprType>& mat, Eigen::TransformTraits traits = Eigen::Affine )
    {
        LineT line = lineArg;
        if (traits==Eigen::Affine)
            line.direction() = mat.inverse().transpose() * line.direction();
        else if (traits==Eigen::Isometry)
            line.direction() = mat * line.direction().homogeneous();
        else
            eigen_assert(0 && "invalid traits value in ParametrizedLine::transform()");

        return line;
    }


// FIXME (AJS): this seems wrong. commenting out to determine if any other projects might use it
//    template<typename LineT, typename Scalar, int AmbientDimAtCompileTime, int TrOptions>
//    inline
//    LineT&
//    transformInPlace( LineT & line, const Eigen::Transform<Scalar,AmbientDimAtCompileTime,Eigen::Affine,TrOptions>& t,Eigen::TransformTraits traits = Eigen::Affine )
//    {
//        transform(t.linear(), traits);
//        line.origin() += t.translation();
//        return line;
//    }

    template<typename LineT, typename XprType>
    inline
    LineT&
    transformHomoInPlace( LineT & line, const Eigen::MatrixBase<XprType>& mat )
    {
        typename LineT::VectorType pt0 = line.origin();
        typename LineT::VectorType pt1 = line.origin() + line.direction();

        pt0 = (mat * pt0.homogeneous()).head(line.dim());
        pt1 = (mat * pt1.homogeneous()).head(line.dim());

        line.origin()    = pt0;
        line.direction() = (pt1 - pt0).normalized();

        return line;
    }


    template<typename LineT, typename XprType>
    inline
    LineT
    transformHomo( const LineT & line, const Eigen::MatrixBase<XprType>& mat )
    {
        LineT outLine;

        typename LineT::VectorType pt0 = line.origin();
        typename LineT::VectorType pt1 = line.origin() + line.direction();

        pt0 = (mat * pt0.homogeneous()).head(line.dim());
        pt1 = (mat * pt1.homogeneous()).head(line.dim());

        outLine.origin()    = pt0;
        outLine.direction() = (pt1 - pt0).normalized();

        return outLine;
    }



    template< typename MatrixT0, typename MatrixT1>
    int insertRowsAt( MatrixT0 & fullMatrix, const MatrixT1 & subMatrix, int startRow )
    {
        fullMatrix.block(startRow,0,subMatrix.rows(),subMatrix.cols()) = subMatrix;
        startRow = startRow + subMatrix.rows();
        return startRow;
    }

    template< typename MatrixT0, typename MatrixT1>
    int insertBlockAt( MatrixT0 & fullMatrix, const MatrixT1 & subMatrix, int startDiag )
    {
        assert( subMatrix.rows() == subMatrix.cols() );
        fullMatrix.block(startDiag,startDiag,subMatrix.rows(),subMatrix.cols()) = subMatrix;
        startDiag = startDiag + subMatrix.rows();
        return startDiag;
    }


    template<typename Derived>
    inline
    bool is_finite(const Eigen::MatrixBase<Derived>& x)
    {
       return ( (x - x).array() == (x - x).array()).all();
    }


    template<typename Derived>
    inline bool
    is_nan(const Eigen::MatrixBase<Derived>& x)
    {
       return !((x.array() == x.array())).all();
    }

    template<typename ScalarT, int Dims>
    inline bool
    is_nan(const Eigen::ParametrizedLine<ScalarT,Dims>& x)
    {
       return is_nan( x.origin() ) || is_nan( x.direction() );
    }

    template<typename VectorT>
    inline bool
    is_nan(const Eigen::LineSegment<VectorT>& x)
    {
       return is_nan( x.p0() ) || is_nan( x.p1() );
    }


    template<typename VectorT>
    Eigen::Quaternion<typename VectorT::Scalar>
    QuaternionFromAxes( const VectorT & xAxis,
                        const VectorT & yAxis,
                        const VectorT & zAxis )
    {
        typedef typename VectorT::Scalar ScalarT;

        Eigen::Matrix<ScalarT,3,3> rotationMatrix;
        rotationMatrix.col(0) = xAxis;
        rotationMatrix.col(1) = yAxis;
        rotationMatrix.col(2) = zAxis;

        return typename Eigen::Quaternion<ScalarT>( rotationMatrix );
    }


    Eigen::VectorXd     toEigen( const std::vector<double> & vector );
    Eigen::VectorXf     toEigen( const std::vector<float> & vector );
    std::vector<double> toStd( const Eigen::VectorXd & vector );
    std::vector<float>  toStd( const Eigen::VectorXf & vector );
    Eigen::VectorXd     clamp( const Eigen::VectorXd & input, const Eigen::VectorXd & mins, const Eigen::VectorXd & maxs );


///// Deprecated // See Eigen namespace of this file for initial-caps replacements
    static const double nanD = std::numeric_limits<double>::quiet_NaN();
    static const Eigen::Vector3d    nan3d( nanD, nanD, nanD );
    static const Eigen::Quaterniond nanQuatd( nanD, nanD, nanD, nanD );
    static const Eigen::Vector3d    zero3d( 0, 0, 0 );

    static const Eigen::Vector3d    xAxis3d( 1, 0, 0 );
    static const Eigen::Vector3d    yAxis3d( 0, 1, 0 );
    static const Eigen::Vector3d    zAxis3d( 0, 0, 1 );
///// End Deprecated

}

#endif /* EIGENHELPER_H_ */
